use strict;
use warnings;
use Test::More 0.96;

use Net::DNS::Git::Zone::Create;

my $zone = Net::DNS::Git::Zone::Create->new(
    zone => 'opndev.io',
    hostmaster => 'hostmaster@opndev.io',
    nameservers => [qw(ns1.opndev.io ns2.opndev.io)],
);

diag $zone->string;

pass("WE ok");

done_testing;
