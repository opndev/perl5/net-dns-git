package Net::DNS::Git::CLI::Create;
use Moo;
use namespace::autoclean;
with 'YA::CLI::ActionRole';

use Data::Dumper;
use feature 'signatures';

sub action    { 'create' }
sub usage_pod { 1 }
use Types::Standard qw(ArrayRef Str Bool);
use List::Util      qw(uniq);
use Net::DNS::Git::Zone::Create;
use File::Spec::Functions qw(catfile);

has nameservers => (
  is       => 'ro',
  isa      => ArrayRef,
  lazy     => 1,
  builder  => '_get_default_name_servers',
  init_arg => 'nameserver'
);

has hostmaster => (
  is      => 'ro',
  isa     => Str,
  lazy    => 1,
  builder => '_get_default_hostmaster',
);

has path => (
  is      => 'ro',
  isa     => Str,
  lazy    => 1,
  builder => 1,
);

has secondary => (
  is      => 'ro',
  isa     => Bool,
  default => 0,
);

has type => (
  is      => 'ro',
  isa     => Str,
  lazy    => 1,
  default =>
    sub { my $self = shift; $self->secondary ? 'secondary' : 'primary' },
);

has bind9domains => (
  is      => 'ro',
  isa     => Str,
  lazy    => 1,
  builder => 1,
);

has primaries => (
  is      => 'ro',
  isa     => ArrayRef,
  lazy    => 1,
  builder => 1,
);

sub cli_options {
  return qw(
    nameserver=s@
    secondary!
    hostmaster=s
    primaries=s@
  );
}

sub _get_default_name_servers {
  return _get_git_config_mvp('nameserver');
}

sub _build_primaries {
  return _get_git_config_mvp('primaries');
}

sub _get_git_config_mvp ($key) {
  my $res = qx{
    git config --get-all gitdns.$key
  };
  return [uniq split(/\n/, $res)];
}

sub _build_path {
  my $self = shift;
  return _get_git_config('paths.' . $self->type,
    catfile("", qw(etc bind), $self->type));
}

sub _build_bind9domains {
  return _get_git_config('paths.local', catfile("", qw(etc bind named.local)));
}

sub _get_default_hostmaster {
  return _get_git_config('hostmaster', 'unknown.hostmaster.example.com');
}

sub _get_git_config ($key, $default = undef) {
  my $res = qx{
    git config --get gitdns.$key
  };
  return $default unless $res;
  $res =~ tr/\r\n//d;
  return $res;
}

sub run {
  my $self = shift;

  die 'No zone(s) given to create', $/ unless @ARGV;

  if (!-d $self->path) {
    die sprintf "Paths %s does not exist!", $self->path;
  }

  foreach (@ARGV) {
    my $file = catfile($self->path, "db.$_");

    if ($self->secondary) {
      $self->_add_zone_config($_, $file);
      next;
    }

    my $zone = Net::DNS::Git::Zone::Create->new(
      zone        => $_,
      hostmaster  => $self->hostmaster,
      nameservers => $self->nameservers,
    );

    if (-f $file) {
      _edit_zone($_, $file);
      next;
    }
    open my $fh, '>', $file;
    print $fh $zone->string;
    close($fh);
    system("git add $file");
    _edit_zone($_, $file);
    $self->_add_zone_config($_, $file);

  }
}

sub _add_zone_config ($self, $zone, $zonepath) {

  my @lines = (
    sprintf('zone "%s" {',  $_),
    sprintf('  type %s;',   $self->type),
    sprintf('  file "%s";', $zonepath), "};"
  );
  if ($self->secondary) {
    my $last = pop(@lines);
    push(@lines,
      sprintf('  primaries { %s; };', join("; ", @{ $self->primaries }))),
      push(@lines, $last);
  }
  my $path = $self->bind9domains;
  open my $fh, '>>', $path;
  print $fh join("\n", "", @lines);
  close($fh);

  my $rc = system("named-checkconf $path");
  if ($rc) {
    warn "named-checkconf failed validation", $/;
    exit($rc >> 8);
  }
  system("git add $path");
}

sub _edit_zone ($zone, $path) {
  my $editor = $ENV{VISUAL} ? $ENV{VISUAL} : $ENV{EDITOR};
  $editor //= 'vim';
  my $rc = system("$editor $path");
  if ($rc) {
    warn "Editor did not exit correctly!", $/;
    exit($rc >> 8);
  }
  $rc = system("named-checkzone $zone $path");
  if ($rc) {
    warn "named-checkzone failed validation", $/;
    exit($rc >> 8);
  }
  system("git add $path");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION
