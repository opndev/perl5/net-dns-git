package Net::DNS::Git::CLI::Roles::GitDir;
use Moo::Role;
use namespace::autoclean;

# ABSTRACT: Do git things

sub ensure_git_dir {
  my $self = shift;
  my $gitdir = qx(git rev-parse --show-toplevel 2>/dev/null);

  if (!$gitdir) {
    qx(git init git-dns);
    chdir 'git-dns';
  }
  else {
    chdir $gitdir;
  }
}

1;

__END__

=head1 SYNOPSIS

  package Foo;
  use Moo;
  with 'Net::DNS::Git::CLI::Roles::GitDir';

  sub run {
    my $self = shift;

    ...;

    $self->ensure_gitdir

    ...;

  }

=head1 DESCRIPTION


=cut
