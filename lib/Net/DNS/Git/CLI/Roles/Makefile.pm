package Net::DNS::Git::CLI::Roles::Makefile;
use Moo::Role;
use namespace::autoclean;

# ABSTRACT: Generate a makefile for your DNS repo

use File::Copy qw(move);
use Types::Standard qw(ArrayRef Str);

has user => (
  is => 'ro',
  isa => Str,
  default => 'bind',
);

has group => (
  is => 'ro',
  isa => Str,
  default => 'bind',
);

sub generate_makefile {
  my $self = shift;

  my $data;
  {
    local $/ = undef;
    $data = <DATA>;
  }

  my $user = $self->user;
  my $group = $self->group;
  $data =~ s/%USER%/$user/;
  $data =~ s/%GROUP%/$group/;

  move('Makefile', 'Makefile.bk') if -f 'Makefile';

  open my $fh, '>', 'Makefile';
  print $fh $data;
  close($fh);
}

=head1 SYNOPSIS

  package Foo;
  use Moo;
  with 'Net::DNS::Git::CLI::Roles::Makefile';

  sub run {
    my $self = shift;

    ...;

    generate_makefile();

    ...;

  }

=head1 DESCRIPTION

Creates/generates a Makefil which takes care of the installation in /etc/bind
for the primary and secundary domains and the named.local file which has the
zone configuration. Everything else is configured via other means, eg Ansible
or other provisioning tools.

TODO: Figure out how to deal with the diverging paths for primary and secundary
zones with respect to the PREFIX and DEST_DIR.

=head1 ATTRIBUTES

=head2 user

The user you want to install the configuration with, defaults to bind.

=head2 group

The user you want to install the configuration with, defaults to bind.

=cut

1;

__DATA__

OUT = ./build

ifeq ($(PREFIX),)
	PREFIX := /etc/bind
endif

ifeq ($(USER),)
	USER := %USER%
endif
ifeq ($(GROUP),)
	GROUP := %USER%
endif

PRIMARY_DNS := $(wildcard primary/db.*)
PRIMARY_TARGETS := $(patsubst primary/%,$(OUT)/primary/%,$(PRIMARY_DNS))

.PHONE: make_directories install all clean $(OUT) $(PRIMARY_TARGETS) $(SECUNDARY_TARGETS)
all: make_directories $(PRIMARY_TARGETS) $(SECUNDARY_TARGETS) build/named.conf.local

make_directories: $(OUT)/ $(OUT)/primary $(OUT)/secundary

$(OUT)/:
	@mkdir -p $@

$(OUT)/primary:
	@mkdir -p $@

$(OUT)/secundary:
	@mkdir -p $@

$(PRIMARY_TARGETS): $(PRIMARY_DNS)
	named-checkzone `basename $< | sed -e "s/^db\.//")` $<
	cp $< $@

clean:
	@rm -rf ./build

install: $(PRIMARY_TARGETS)
	install -m 750 -d -o $(USER) -g $(GROUP) $(DEST_DIR)$(PREFIX)/primary
	install -m 440 -o $(USER) -g $(GROUP) $(OUT)/primary/* -t $(DEST_DIR)$(PREFIX)/primary
	mkdir -p $(DEST_DIR)$(PREFIX)/secundary
	install -m 444 $(OUT)/named.conf.local $(DEST_DIR)$(PREFIX)/named.conf.local

build/named.conf.local: named.local
	named-checkconf $<
	sed -e "s#file \"\(.\+\)\"#file \"$(PREFIX)/\1\"#" $< > $@
