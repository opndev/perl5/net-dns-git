package Net::DNS::Git::CLI::Init;
use Moo;
use namespace::autoclean;
with qw(
  YA::CLI::ActionRole
  Net::DNS::Git::CLI::Roles::Makefile
  Net::DNS::Git::CLI::Roles::GitDir
);

# ABSTRACT: Initialize a git-dns repo

use Data::Dumper;
use feature 'signatures';

sub action    { 'init' }
sub usage_pod { 1 }
use File::Copy qw(move);
use Types::Standard qw(ArrayRef Str);

has hostmaster => (
  is => 'ro',
  isa => Str,
  required => 1,
);

has nameservers => (
  is => 'ro',
  isa => ArrayRef,
  required => 1,
  init_arg => 'nameserver',
);

has primaries => (
  is => 'ro',
  isa => ArrayRef,
  required => 1,
  init_arg => 'primary',
);

sub cli_options {
  return qw(
    user=s
    group=s
    hostmaster=s
    nameserver=s@
    primary=s@
  );
}

sub run {
  my $self = shift;

  $self->ensure_gitdir;
  $self->generate_makefile();

  my @array = @{$self->nameservers};
  my $first = shift @array;
  qx(git config --local --replace-all gitdns.nameserver $first);
  foreach (@array) {
    qx(git config --local --add gitdns.nameserver $_);
  }

  @array = @{$self->primaries};
  $first = shift @array;
  qx(git config --local --replace-all gitdns.primaries $first);
  foreach (@array) {
    qx(git config --local --add gitdns.primaries $_);
  }

  my $hostmaster = $self->hostmaster;
  qx(git config --local --replace-all gitdns.hostmaster $hostmaster);

  qx(git config --local gitdns.path.local named.local);
  qx(git config --local gitdns.path.primary primary);
  qx(git config --local gitdns.path.secundary secundary);

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SYNOPSIS

  git dns init [ OPTIONS ] [ --help ] [ --man ]


=head1 DESCRIPTION

Initializes the git DNS repo. When you aren't in a git controlled directory,
the git directory will be created for you. The various options determine data
which is configurable. These are all saved in the git-dir of this project and
are not in version control. These are mostly used for creating new zonefiles.
The zones can be editted to your hearts content and are stored in git.

The Makefile which is generated takes care of the installation in /etc/bind for
the primary and secundary domains and the named.local file which has the zone
configuration. Everything else is configured via other means, eg Ansible or
other provisioning tools.

=head1 OPTIONS

=head2 help

This help

=head2 man

The manual page, a more in depth document

=head2 hostmaster

The default hostmaster for your domains

=head2 nameserver

The default nameserver for your domains. You can add as many as you like

=head2 primaries

The default primaries for your domains. You can add as many as you like

=head2 user

The user you want to install the configuration with, defaults to bind.

=head2 group

The user you want to install the configuration with, defaults to bind.

=head1 SEE ALSO

=head2 git dns init makefile

=head2 git dns init config
