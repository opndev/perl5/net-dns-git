package Net::DNS::Git::CLI::Init::Makefile;
use Moo;
use namespace::autoclean;
with qw(
  YA::CLI::ActionRole
  Net::DNS::Git::CLI::Roles::Makefile
  Net::DNS::Git::CLI::Roles::GitDir
);

# ABSTRACT: Initialize a git-dns repo

use Data::Dumper;
use feature 'signatures';

sub action    { 'init' }
sub subaction { 'makefile' }
sub usage_pod { 1 }
use File::Copy qw(move);
use Types::Standard qw(ArrayRef Str);

sub cli_options {
  return qw(
    user=s
    group=s
  );
}

sub run {
  my $self = shift;

  $self->ensure_git_dir;
  $self->generate_makefile();

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SYNOPSIS

  git dns init makefile [ OPTIONS ] [ --help ] [ --man ]


=head1 DESCRIPTION

Initializes the git DNS repo with a Makefile. When you aren't in a git
controlled directory, the git directory will be created for you.

The Makefile which is generated takes care of the installation in /etc/bind for
the primary and secundary domains and the named.local file which has the zone
configuration. Everything else is configured via other means, eg Ansible or
other provisioning tools.

=head1 OPTIONS

=head2 help

This help

=head2 man

The manual page, a more in depth document

=head2 user

The user you want to install the configuration with, defaults to bind.

=head2 group

The user you want to install the configuration with, defaults to bind.
