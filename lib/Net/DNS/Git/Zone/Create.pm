package Net::DNS::Git::Zone::Create;
use v5.26;
use Object::Pad;

# ABSTRACT: Create the template of a zone file

class Net::DNS::Git::Zone::Create;
use Carp qw(croak);

use Data::Dumper;
use Net::DNS::RR;
use DateTime;
use DateTime::Format::Strptime;

field $zone       :param;
field @nameservers;
field $hostmaster :param;
field $serial     :param = undef;
field $refresh    :param = 28800;
field $retry      :param = 7200;
field $expire     :param = 604800;
field $minimum    :param = 3600;
field $ttl        :param = 86400;
field $tz         :param = $ENV{TZ};

field $soa;

ADJUSTPARAMS {
    my $args = shift;

    croak "No nameservers defined!" unless ref $args->{nameservers};
    my $nameservers = delete $args->{nameservers};
    croak "Nameservers are should be an array" if ref $nameservers ne 'ARRAY';

    # TODO validate their syntax
    @nameservers = @$nameservers;
}

ADJUST {

    $serial //= DateTime->now()->ymd('') . '01' + 0;
    unless ($tz) {
        unless (-f '/etc/timezone') {
            $tz = 'UTC';
        }
        else {
            open my $fh, '<', '/etc/timezone';
            local $/ = undef;
            $tz = <$fh>;
            $tz =~ tr/\r\n//d;
        }
    }

    # Strip last dot from zone
    $zone =~ s/\.$//;
    $hostmaster =~ s/\.$//;

    $soa = Net::DNS::RR->new(
      type    => 'SOA',
      owner   => $zone,
      rname   => $hostmaster,
      mname   => $nameservers[0],

      ttl     => $ttl,

      refresh => $refresh,
      retry   => $retry,
      expire  => $expire,
      minimum => $minimum,
      serial  => $serial + 0,
    );
}

method string {
    my $scalar = $soa->string;
    my $dt = DateTime->now();
    $dt->set_time_zone($tz);

    my @rdata;

    my %map = (
      serial  => $serial,
      refresh => $refresh,
      expire  => $expire,
      minimum => $minimum,
      retry   => $retry
    );
    foreach (qw(serial refresh retry expire minimum)) {
        my $val = $map{$_};
        my $spacer = length "$val" > 7 ? "" : "\t";
        push(@rdata, sprintf("\t\t\t\t%s%s\t;%s", $val, $spacer, $_));
    }
    my @lines = (
        sprintf("; %s created at %s", $zone,
          $dt->format_cldr(q{ccc LLL d HH:MM:ss VVV YYYY})),
        "",
        "\$TTL $ttl",
        sprintf("@\t\tIN\tSOA\t%s %s (", $soa->{mname}->string, $soa->{rname}->string),
        @rdata,
        "\t\t\t)",
    );

    foreach (@nameservers) {
        my $ns = $_ =~ s/\.?$//r;
        push(@lines, "\t\t\tIN\tNS\t$ns.");
    }
    push(@lines, "; Add your zone entries below this line", "");
    return join("\n", @lines);
}

1;

__END__

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 ATTRIBUTES

=head1 METHODS
__DATA__

; %ZONE% created at %CREATED_AT%
$TTL %TTL$
@               IN  SOA %NAMESERVER%. %HOSTMASTER%. (
                            %SERIAL% ; Serial
                            28800      ; Refresh 8 hours
                            7200       ; Retry   2 hours
                            604800     ; Expire   7 days
                            3600       ; Minimum 1 hour
                        )

                IN  NS      %NAMESERVER%

                IN  A       %IP%
                IN  MX      50 %MX%
                IN  TXT     %SPF%

; -- Name servers
ns1             IN  A       95.211.147.230
; Leaseweb: https://secure.leaseweb.com/services/domains/secondary.asp
ns3             IN  A       62.212.76.50

spf             IN  TXT    "v=spf1 a:senders.opperschaap.net include:tweakmail.spf.cambrium.nl include:spf.efwd.registrar-servers.com include:node.capitar.com -all"

senders         IN  A       95.211.147.230
                IN  A       95.211.218.103
                IN  A       186.189.151.106
