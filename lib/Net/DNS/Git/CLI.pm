package Net::DNS::Git::CLI;
our $VERSION = '0.001';
use Moo;
use namespace::autoclean

extends 'YA::CLI';

sub default_handler { undef };

sub exclude_search_path {
  return qr/^Net::DNS::Git::CLI::Roles/;
}

1;

